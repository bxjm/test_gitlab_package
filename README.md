# test_gitlab_package
A test project for GitLab package ...

## 0、map of knowledge
![about_php_composer](https://gitlab.com/bxjm/test_gitlab_package/-/raw/main/about_php_composer.jpg)

## 1、prepare
- public group
- public repo

## 2、publish

##### 2.1、create a new tag
```shell
$ git tag 0.1.1
```
##### 2.2、publish a new package
```shell
$ curl --location --request POST 'https://gitlab.com/api/v4/projects/<project_id>/packages/composer' \
--header 'PRIVATE-TOKEN: ******' \
--form 'tag="0.1.1"'
```

## 3、install

##### 3.1、config `composer.json`
```json
{
    ...
    "repositories": [
        {
            "type": "composer",
            "url": "https://gitlab.com/api/v4/group/<group_id>/-/packages/composer/packages.json"
        }
    ],
    ...
}
```

##### 3.2、install
```shell
$ composer req bxjm/test_gitlab_package
```

##### 3.3、require
```php
<?php
    require_once 'vendor/autoload.php';
    $test = new \bxjm\TestGitlabPackage\SomeClass();
    echo $test->someMethod();
?>
```

## 4、Related links
##### 4.1、GitLab Packages
- https://docs.gitlab.com/ee/user/packages/
- https://docs.gitlab.com/ee/api/personal_access_tokens.html
- https://docs.gitlab.com/ee/api/packages/composer.html#create-a-package
- https://docs.gitlab.com/ee/user/packages/composer_repository/
##### 4.2 Composer
- https://getcomposer.org/doc/
- https://getcomposer.org/doc/04-schema.md#version
- https://getcomposer.org/doc/05-repositories.md#types
